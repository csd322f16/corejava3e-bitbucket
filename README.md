# README #

Source code from the csd322 textbook, 3rd edition

### What is this repository for? ###

* Example code form the textbook
* 3rd edition

### How do I get set up? ###

* download with sourcetree then open a project group in Netbeans

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Fred Carella, fred.carella@saultcollege.ca